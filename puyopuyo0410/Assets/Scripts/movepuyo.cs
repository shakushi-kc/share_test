﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movepuyo : MonoBehaviour {

    public float start_x = 0;
    public float start_y = 0;
    public float TIME_DISTANCE = 1.0f;
    public float MOVE_DIS_X = 1;
    public float MOVE_DIS_Y = 1;
    private float COOL_TIME = 0.5f;
    private float check_mtime = 0;
    private float check_ctime = 0;
    private bool is_ctime = false;
    private float left_max = -10;
    private float right_max = 10;
    private static float bottom = -15;
    //private int cnt = 0;

	// Use this for initialization
	void Start () {
        var pos = transform.position;
        pos.x = start_x;
        pos.y = start_y;
        transform.position = pos;
    }
	
	// Update is called once per frame
	void Update () {
        if(isMoveTime() == true)
        {
            var pos = transform.position;
            pos.y = Mathf.Clamp(pos.y - MOVE_DIS_Y, bottom, start_y);
            transform.position = pos;
        }
        if (isCoolTime() == false)
        {
            MoveByKey();
        }

    }

    //左右もしくは↓のキーを受け取って移動する関数
    private void MoveByKey()
    {
        Transform myTransform = this.transform;
        Vector2 pos = myTransform.position;
        if(Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            pos.x = Mathf.Clamp(pos.x - MOVE_DIS_X, left_max, right_max);
            is_ctime = true;
            Debug.Log("LEFT_MOVE");
        }
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            pos.x = Mathf.Clamp(pos.x + MOVE_DIS_X, left_max, right_max);
            is_ctime = true;
        }
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            pos.y = Mathf.Clamp(pos.y - MOVE_DIS_Y, bottom, start_y);
            is_ctime = true;
            check_ctime += 0.25f;
        }
        //4-12 update
        if (Input.GetKey(KeyCode.Space))
        {
            //cnt++;
            Debug.Log("Rotate");
            float x = 90;
            this.transform.Rotate(0.0f,0.0f,x);
            is_ctime = true;
            check_ctime += 0.25f;
        }
        myTransform.position = pos;
        return;
    }

    //ぷよが落ちるタイミングかどうかの判定
    private bool isMoveTime()
    {
        check_mtime += Time.deltaTime;
        if (check_mtime > TIME_DISTANCE)
        {
            check_mtime = 0;
            return true;
        }
        return false;
    }

    //キー操作を受け付けない時間（クールタイム）かどうかの判定
    private bool isCoolTime()
    {
        check_ctime += Time.deltaTime;
        if (check_ctime > TIME_DISTANCE)
        {
            check_ctime = 0;
            is_ctime = false;
        }
        return is_ctime;
    }
}
